output "aks_service_principal" {
  value = "${azuread_service_principal.aks_app_service_principal.application_id}"
}