variable "location" {
  default = "westeurope"
}

variable "project_name" {
  default = "aks-accelerator"
}

variable "tags" {
  type = "map"

  default = {
    "terraform" = true
  }
}

variable "service_principal_password" {}