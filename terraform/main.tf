provider "azurerm" {}
provider "azuread" {}

module "accelerator-service-principal" {
  source                     = "accelerator-service-principal"
  project_name               = "${var.project_name}"
  service_principal_password = "${var.service_principal_password}"
}

module "accelerator-network" {
  source       = "accelerator-network"
  project_name = "${var.project_name}"
  location     = "${var.location}"
}

module "accelerator-aks" {
  source                     = "accelerator-aks"
  project_name               = "${var.project_name}"
  location                   = "${var.location}"
  aks_vnet_subnet_id         = "${module.accelerator-network.aks_vnet_subnet_id}"
  service_principal          = "${module.accelerator-service-principal.aks_service_principal}"
  service_principal_password = "${var.service_principal_password}"
}

module "accelerator-diagnostics" {
  source                         = "accelerator-diagnostics"
  aks_cluster_id                 = "${module.accelerator-aks.aks_cluster_id}"
  aks_resource_group_name        = "${module.accelerator-aks.aks_resource_group_name}"
  project_name                   = "${var.project_name}"
  location                       = "${var.location}"
  aks_log_analytics_workspace_id = "${module.accelerator-aks.aks_log_analytics_workspace_id}"
}
